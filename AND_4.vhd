library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity AND_4 is
    Port ( x0 : in  	STD_LOGIC;
           x1 : in  	STD_LOGIC;
			  x2 : in	STD_LOGIC;
			  x3 : in 	STD_LOGIC;
           F : out  	STD_LOGIC);
end AND_4;

architecture Behavioral of AND_4 is

begin

F <= x0 and x1 and x2 and x3;

end Behavioral;

