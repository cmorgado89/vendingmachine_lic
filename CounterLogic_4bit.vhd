library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity CounterLogic_4bit is
    Port ( operandA : in  STD_LOGIC_VECTOR (3 downto 0);
           en : in  STD_LOGIC;
           R : in  STD_LOGIC_VECTOR (3 downto 0));
end CounterLogic_4bit;

architecture Structural of CounterLogic_4bit is
component adder_4bit is
    Port ( A : in  STD_LOGIC_VECTOR (3 downto 0);
           B : in  STD_LOGIC_VECTOR (3 downto 0);
           Cin : in  STD_LOGIC;
           S : out  STD_LOGIC_VECTOR (3 downto 0);
           Cout : out  STD_LOGIC);
end component;

begin

U0:	adder_4bit
		port map(A => operandA, B=> '1', Cin => '0', s=> R, Cout => open);

end Structural;

