library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity MUX4_1 is
generic (
		WIDTH : POSITIVE := 1
	);
    Port (	I0 : in  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
				I1 : in  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
				I2 : in  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
				I3 : in  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
				S0 : in STD_LOGIC;
				S1 : in STD_LOGIC;
				Y : out  STD_LOGIC_VECTOR (WIDTH-1 downto 0));
end MUX4_1;

architecture Behavioral of MUX4_1 is

begin

	Y <= 	I0 when (S0 = '0' and S1 = '0') -- Q : XX00
			else I1 when (S0 = '1' and S1 = '0') -- Q : XX01
			else I2 when (S0 = '0' and S1 = '1') -- Q : XX10
			else I3 when (S0 = '1' and S1 = '1'); -- Q : XX11

end Behavioral;

