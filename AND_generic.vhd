library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity AND_generic is
generic (
		WIDTH : POSITIVE := 1
	);
    Port ( A : in  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
           B : in  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
           F : out  STD_LOGIC);
end AND_generic;

architecture Behavioral of AND_generic is

begin

F <= A and B;

end Behavioral;

