----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:13:48 10/11/2018 
-- Design Name: 
-- Module Name:    KeyDecode - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity KeyDecode is
    Port ( CLK : in  STD_LOGIC;
           RST : in  STD_LOGIC;
			  row : in STD_LOGIC_VECTOR(3 downto 0);
           Kack : in  STD_LOGIC;
			  col : out STD_LOGIC_VECTOR(2 downto 0);
           Kval : out  STD_LOGIC;
			  K : out STD_LOGIC_VECTOR(3 downto 0));
end KeyDecode;

architecture Structural of KeyDecode is

component KeyScan is
    Port ( CLK : in  STD_LOGIC;
           Kscan : in  STD_LOGIC;
           K : out  STD_LOGIC_VECTOR (3 downto 0);
           Kpress : out  STD_LOGIC;
           colVals : out  STD_LOGIC_VECTOR (2 downto 0);
           rowVals : in  STD_LOGIC_VECTOR (3 downto 0));
end component;

component KeyControl is
    Port ( CLK : in STD_LOGIC;
			  RST : in STD_LOGIC;
			  Kpress : in  STD_LOGIC;
			  Kack : in  STD_LOGIC;
           Kscan : out  STD_LOGIC;
           Kval : out  STD_LOGIC);
end component;

signal Kscan : STD_LOGIC;
signal Kpress : STD_LOGIC;
signal NOTCLOCK : STD_LOGIC;

begin

U0 : KeyScan
		Port Map (CLK => CLK, Kscan => Kscan, K => K, Kpress => Kpress, colVals => col, rowVals => row);
		
U1 : KeyControl
		Port Map (CLK => NOTCLOCK, RST => RST, Kpress => Kpress,  Kack => Kack, Kscan => Kscan, Kval => Kval);
		
NOTCLOCK <= not CLK;

end Structural;

