library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity register_D is
	generic (
		WIDTH : POSITIVE := 1
	);
    Port ( CLK : in  STD_LOGIC;
			  RST : in STD_LOGIC;
           D : in  STD_LOGIC_VECTOR(WIDTH-1 downto 0);
           Q : out  STD_LOGIC_VECTOR(WIDTH-1 downto 0);
			  EN : in STD_LOGIC
	);
end register_D;

architecture Behavioral of register_D is

begin

	process (CLK, RST, EN)
	begin
		if (RST='1') then
			Q <= (others=>'0');
		elsif (rising_edge(CLK) and EN = '1') then
			Q <= D;
		end if;
	end process;

end Behavioral;