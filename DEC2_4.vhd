library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity DEC2_4 is
    Port ( A : in  STD_LOGIC_VECTOR (1 downto 0);
           X0 : out  STD_LOGIC;
			  X1 : out 	STD_LOGIC;
			  X2 : out	STD_LOGIC);
end DEC2_4;

architecture Behavioral of DEC2_4 is

begin

	X0 <= '0' when A="00" else '1';
	X1 <= '0' when A="01" else '1';
	X2 <= '0' when A="10" else '1';			
	
end Behavioral;
