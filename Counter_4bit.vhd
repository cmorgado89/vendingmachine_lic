library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Counter_4bit is
	Port(	clk : in  STD_LOGIC;
			en : in  STD_LOGIC;
			q : out  STD_LOGIC_VECTOR (3 downto 0));
end Counter_4bit;

architecture Structural of Counter_4bit is

component register_D is
	generic (
		WIDTH : POSITIVE := 1
	);
    Port ( CLK : in  STD_LOGIC;
			  RST : in STD_LOGIC;
           D : in  STD_LOGIC_VECTOR(WIDTH-1 downto 0);
           Q : out  STD_LOGIC_VECTOR(WIDTH-1 downto 0);
			  EN : in STD_LOGIC
	);
end component;

component adder_4bit is
    Port ( A : in  STD_LOGIC_VECTOR (3 downto 0);
           B : in  STD_LOGIC_VECTOR (3 downto 0);
           Cin : in  STD_LOGIC;
           S : out  STD_LOGIC_VECTOR (3 downto 0);
           Cout : out  STD_LOGIC);
end component;

component AND_4 is
    Port ( x0 : in  	STD_LOGIC;
           x1 : in  	STD_LOGIC;
			  x2 : in	STD_LOGIC;
			  x3 : in 	STD_LOGIC;
           F : out  	STD_LOGIC);
end component;

component MUX2_1 is
generic (
		WIDTH : POSITIVE := 1
	);
    Port ( I0 : in  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
           I1 : in  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
           sel : in  STD_LOGIC;
           Y : out  STD_LOGIC_VECTOR (WIDTH-1 downto 0));
end component;

signal counter, sumTotal, newCount : std_logic_vector(3 downto 0);
signal countReset : std_logic := '1';

begin

U0: adder_4bit
		port map( A=>counter, B=> "0001", Cin=>'0', S=> sumTotal, Cout => open); -- Soma um em cada ciclo de clock
		
U2: MUX2_1
		generic map( WIDTH => 4)
		port map (I0=> sumTotal, I1=> "0000", sel => countReset, Y => newCount);

U3: register_D
		generic map( WIDTH => 4)
		port map ( CLK => clk, RST => '0', D=> newCount, Q => counter, EN => en);
		
U4:	AND_4
		port map ( x0 => counter(0), x1 => counter(1), x2 => NOT counter(2), x3 => counter(3), F => countReset);
		
q <= counter;


end Structural;

