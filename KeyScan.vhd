library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity KeyScan is
    Port ( CLK : in  STD_LOGIC;
           Kscan : in  STD_LOGIC;
           K : out  STD_LOGIC_VECTOR (3 downto 0);
           Kpress : out  STD_LOGIC;
           colVals : out  STD_LOGIC_VECTOR (2 downto 0);
           rowVals : in  STD_LOGIC_VECTOR (3 downto 0));
end KeyScan;

architecture Structural of KeyScan is

component MUX4_1 is
generic (
		WIDTH : POSITIVE := 1
	);
    Port (	I0 : in  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
				I1 : in  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
				I2 : in  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
				I3 : in  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
				S0 : in STD_LOGIC;
				S1 : in STD_LOGIC;
				Y : out  STD_LOGIC_VECTOR (WIDTH-1 downto 0));
end component;

component Counter_4bit is
	Port(	clk : in  STD_LOGIC;
			en : in  STD_LOGIC;
			q : out  STD_LOGIC_VECTOR (3 downto 0));
end component;

component DEC2_4 is
    Port ( A : in  STD_LOGIC_VECTOR (1 downto 0);
           X0 : out  STD_LOGIC;
			  X1 : out 	STD_LOGIC;
			  X2 : out	STD_LOGIC);
end component;

signal col : STD_LOGIC_VECTOR (2 downto 0);
signal Q : STD_LOGIC_VECTOR (3 downto 0); -- Sa�da K
signal keyPress : STD_LOGIC; -- Saida bloco - KPress

begin

U0:	MUX4_1
		generic map( WIDTH => 1)
		Port map(I0(0) => rowVals(0), I1(0) => rowVals(1), I2(0) => rowVals(2), I3(0) => rowVals(3), S0 => Q(0), S1 => Q(1), Y(0) =>keyPress);

U1:	DEC2_4
		Port map(A(0) => Q(2), A(1) => Q(3), X0 => col(0), X1 => col(1), X2 => col(2));

U2:	Counter_4bit
		Port map(clk=> CLK, en => Kscan, q => Q);
		
Kpress <= NOT keyPress;
K <= Q;
colVals <= col;
end Structural;