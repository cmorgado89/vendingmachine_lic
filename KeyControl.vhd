----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:38:42 10/11/2018 
-- Design Name: 
-- Module Name:    KeyControl - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity KeyControl is
    Port ( CLK : in STD_LOGIC;
			  RST : in STD_LOGIC;
			  Kpress : in  STD_LOGIC;
			  Kack : in  STD_LOGIC;
           Kscan : out  STD_LOGIC;
           Kval : out  STD_LOGIC);
end KeyControl;

architecture Behavioral of KeyControl is

type STATE_TYPE is (ST_READ, ST_PRESS, ST_ACK); 

signal CS, NS : STATE_TYPE;

begin

State_Transitions : process (CLK, RST)
	begin
		if(RST = '1') then
			CS <= ST_READ;
		elsif rising_edge(CLK) then
			CS <= NS;
		end if;
	end process;


Next_State_Evaluation : process (CS, Kpress, Kack)
begin
	case (CS) is
	when ST_READ	=> if(Kpress = '1') then NS <= ST_PRESS;
							else NS <= ST_READ;
							end if;
	when ST_PRESS	=> if(Kack = '1') then NS <= ST_ACK;
							else NS <= ST_PRESS;
							end if;
	when ST_ACK		=> if(Kack = '0' and Kpress = '0') then NS <= ST_READ;
							else NS <= ST_ACK;
							end if;
	end case;
end process;

Kscan <= '1' when (CS = ST_READ) else '0';
Kval <= '1' when (CS = ST_PRESS) else '0';

end Behavioral;

